/*
 * Copyright (c) Vertsimaha Systems, Inc.
 * This is the test Menu for the home task
 */

import com.taskfibonacci.TaskOne;
import com.taskfibonacci.TaskTwo;
import java.util.Scanner;

/**
 * This class was created for tests
 *
 * @author Andrii Vertsimaha
 * @version 1.0
 * @since 2018-11-11
 */

public class Menu {

  /**
   * main method
   */
  public static void main(String[] args) {
    //just start a program
    startProgram();
  }

  /**
   * private method for test task one
   */
  private static void testTaskOne() {
    TaskOne taskOne = new TaskOne();
    taskOne.getOddNumbers();
    taskOne.getEvenNumbers();
  }

  /**
   * private method for test task two
   */
  private static void testTaskTwo() {
    TaskTwo taskTwo = new TaskTwo();
    taskTwo.getTaskTwo();
  }

  /**
   * private method which contain parts of interface
   */
  private static void startProgram() {
    System.out.println(
        "Choose task you want to execute:\n"
            + "1. Odd and even numbers\n"
            + "2. Fibonacci array\n"
            + "3. Exit\n\n"
            + "Please enter number (1, 2 or 3): "
    );
    switch (new Scanner(System.in).nextInt()) {
      case 1:
        testTaskOne();
        startProgram();
        break;
      case 2:
        testTaskTwo();
        startProgram();
        break;
      case 3:
        System.exit(0);
      default:
        System.out.print("It's not correct! Please try again or exit\n"
            + "1. Try again\n"
            + "2. Exit\n"
            + "Choose (1 or 2): ");

        switch (new Scanner(System.in).nextInt()) {
          case 1:
            startProgram();
            break;
          case 2:
            break;
          default:
            break;
        }

        break;
    }

  }
}
