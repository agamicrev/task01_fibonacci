package com.taskfibonacci;

import java.io.Closeable;
import java.io.IOException;
import java.util.Scanner;

/**
 * Custom scanner class with realized Closeable interface.
 *
 * @author Andrii Vertsimaha
 */
public class CustomScanner implements Closeable {

  private Scanner scanner = null;

  public CustomScanner() {
    scanner = new Scanner(System.in);
  }

  public boolean hasNext() {
    return scanner.hasNext();
  }

  /**
   * Method overrided from Closeable interface.
   *
   * @throws IOException - default exception int method signature
   * @throws RuntimeException - just cause
   */
  public void close() throws IOException {
    scanner.close();
  }

  /**
   * nextLine method.
   * @return scanner.nextLine()
   */
  public String nextLine() {
    return scanner.nextLine();
  }

  /**
   * nextInt method.
   * @return scanner.nextInt()
   */
  public int nextInt() {
    return scanner.nextInt();
  }
}

