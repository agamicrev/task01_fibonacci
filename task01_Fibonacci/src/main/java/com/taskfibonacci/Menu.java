/*
 * Copyright (c) Vertsimaha Systems, Inc.
 * This is the Menu for home task
 */
package com.taskfibonacci;

import java.util.Scanner;
import java.io.IOException;

/**
 * This class Menu for home task.
 */
public class Menu {

  /**
   * This constructor prevents calls from subclass.
   */
  protected Menu() {
    throw new UnsupportedOperationException();
  }

  /**
   * This is a main method.
   *
   * @param args - default
   */
  public static void main(final String[] args) {
    //just start a program
    startProgram();
  }

  /**
   * Private method for execution task one.
   */
  private static void executeTaskOne() {
    TaskOne taskOne = new TaskOne();
    taskOne.getOddNumbers();
    taskOne.getEvenNumbers();
  }

  /**
   * Private method for execution task two.
   */
  private static void executeTaskTwo() {
    TaskTwo taskTwo = new TaskTwo();
    taskTwo.getTaskTwo();
  }

  /**
   * Private method which contains Menu.
   */
  private static void startProgram() {
    try (CustomScanner scanner = new CustomScanner()) {
      System.out.print(
          "\nChoose task you want to execute:\n"
              + "1. Odd and even numbers\n"
              + "2. Fibonacci array\n"
              + "3. Exit\n\n"
              + "Please enter number (1, 2 or 3): "
      );
      switch (new Scanner(System.in).nextInt()) {
        case 1:
          executeTaskOne();
          startProgram();
          break;
        case 2:
          executeTaskTwo();
          startProgram();
          break;
        case 3:
          System.exit(0);
        default:
          System.out.print("It's not correct! Please try again or exit\n"
              + "1. Try again\n"
              + "2. Exit\n"
              + "Choose (1 or 2): ");

          switch (new Scanner(System.in).nextInt()) {
            case 1:
              startProgram();
              break;
            case 2:
              break;
            default:
              break;
          }

          break;
      }
    } catch (IOException e) {
    }
  }
}

