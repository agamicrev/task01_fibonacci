/*
 * Copyright (c) Vertsimaha Systems, Inc.
 * This software is the second part of home task
 */
package com.taskfibonacci;

import java.util.Scanner;

/**
 * This class was created for implement second part of homework. Program
 * build Fibonacci numbers: F1 will be the biggest odd number and F2  -
 * the biggest even number. User can enter the size of set(N).
 * Program prints percentage of odd and even Fibonacci numbers.
 *
 * @author Andrii Vertsimaha
 * @version 1.0
 * @since 2018-11-11
 */

public class TaskTwo {

  /**
   * Variable for containing 100%.
   */
  static final double ONE_HUNDRED_PERCENT = 100.0;

  /**
   * Variable for containing 10 for rounding.
   */
  static final int T = 10;

  /**
   * Method prints interface for second task.
   */
  public final void getTaskTwo() {
    System.out.print("Please write number of set Fibonacci array:\n"
        + "N = ");

    fibonacci(new Scanner(System.in).nextInt());
  }

  /**
   * Method prints set of Fibonacci numbers.
   *
   * @param n - count of Fibonacci numbers which must printed
   */
  public final void fibonacci(final int n) {

    int temp; // temporary variable for containing number of Fibonacci array
    int previousNumber = 0;  // contains previous number of Fibonacci array
    int currentNumber = 1;   // contains current number of Fivonacci array
    int odds = 0;            // count of odd numbers
    int evens = 0;           // count of even numbers
    double oddsPercent = 0;  // percent of odd numbers
    double evensPercent = 0; // percent of even numbers
    int f1 = 0;              // F1 - the biggest odd number
    int f2 = 0;              // F2 – the biggest even number

    System.out.println("Your array:");

    for (int i = 0; i < n; i++) {
      //check if current number is odd or even
      if (currentNumber % 2 == 0) {
        evens++;
        f2 = currentNumber;
      } else {
        odds++;
        f1 = currentNumber;
      }

      System.out.print(currentNumber + " ");
      temp = currentNumber;
      currentNumber += previousNumber;
      previousNumber = temp;

    }

    // count percent of odd and even numbers
    oddsPercent = odds / (n / ONE_HUNDRED_PERCENT);
    evensPercent = evens / (n / ONE_HUNDRED_PERCENT);

    // round values of odd and even numbers
    oddsPercent = Math.round(oddsPercent * Math.pow(T, 2)) / Math.pow(T, 2);
    evensPercent = Math.round(evensPercent * Math.pow(T, 2)) / Math.pow(T, 2);

    System.out.println("\nF1 (the biggest odd number): " + f1);
    System.out.println("F2 (the biggest even number): " + f2);

    System.out.println("We have " + oddsPercent + "%" + " of odd numbers");
    System.out.println("We have " + evensPercent + "%" + " of even numbers");
  }

}

