/*
 * Copyright (c) Vertsimaha Systems, Inc.
 * This software is the first part of home task
 */
package com.taskfibonacci;

import java.util.Scanner;

/**
 * This class was created for implement first part of homework.
 * User enter the interval (for example: [1;100]). Program prints odd
 * numbers from start to the end of interval and even numbers from end
 * to start. Program prints the sum of odd and even numbers.
 *
 * @author Andrii Vertsimaha
 * @version 1.0
 * @since 2018-11-11
 */
public class TaskOne {

  /**
   * Variable for containing first number of interval.
   */
  private int start;
  /**
   * Variable for containing second number of interval.
   */
  private int end;

  /**
   * Method prints odd numbers from start to the end of interval.
   */
  public final void getOddNumbers() {
    Scanner scanner = new Scanner(System.in); // get numbers from console
    int sumOfOdds = 0;                        // sum of odd numbers

    System.out.print("Please enter interval for odd numbers: \n" + "From - ");
    start = scanner.nextInt(); // set first number of interval

    System.out.print("To - ");
    end = scanner.nextInt();   // set second number of interval

    System.out.println("Your odd numbers are:");

    // loop give us sum and print array of odd numbers
    for (int i = start; i <= end; i++) {
      if (i % 2 != 0) {
        System.out.print(i + " ");
        sumOfOdds += i;
      }
    }

    System.out.println("\nSum of odd numbers are - " + sumOfOdds);
  }

  /**
   * Method prints even numbers from end to the start of interval.
   */
  public final void getEvenNumbers() {

    Scanner scanner = new Scanner(System.in); // get numbers from console
    int sumOfEven = 0;                        // sum of even numbers

    System.out.print("Please enter interval for even numbers: \n" + "From - ");
    start = scanner.nextInt();                // set first number of interval

    System.out.print("To - ");
    end = scanner.nextInt();                  // set second number of interval

    System.out.println("Your  even numbers are: ");

    // Loop give us sum and print array of even numbers
    for (int i = end; i >= start; i--) {
      if (i % 2 == 0) {
        System.out.print(i + " ");
        sumOfEven += i;
      }
    }

    System.out.println("\nSum of even numbers are - " + sumOfEven);
  }

}
